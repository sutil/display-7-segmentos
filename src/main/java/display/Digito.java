package display;

import java.util.HashMap;
import java.util.Map;

public enum Digito {
	
	ZERO("0", new Impressor0()),
	UM("1", new Impressor1()),
	DOIS("2", new Impressor2());
	
	private String valor;
	private Impressor impressor;

	private Digito(String valor, Impressor impressor){
		this.valor = valor;
		this.impressor = impressor;
	}
	
	
	private static Map<String, Digito> digitos = new HashMap<String, Digito>();
	static {
		for(Digito d : values())
			digitos.put(d.valor, d);
			
	}

	public static Digito fromValue(String value) {
		return digitos.get(value);
	}
	
	public Impressor getImpressor() {
		return impressor;
	}

}
