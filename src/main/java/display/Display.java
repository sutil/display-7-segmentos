package display;

import java.util.ArrayList;
import java.util.List;

public class Display {

	private String valor;
	
	private List<Digito> digits = new ArrayList<Digito>();

	public Display(String valor) {
		this.valor = valor;
		inicializaDigitos();
	}

	public String imprime() {		
		StringBuilder builder = new StringBuilder();
		
		for(int linha = 0; linha < 3; linha++) {
			for(Digito d : this.digits) {
				builder.append(d.getImpressor().linha(linha));
			}
			builder.append(System.lineSeparator());
		}
		
		return builder.toString();
	}

	private void inicializaDigitos() {
		for(String digito : this.getDigits())
			this.digits.add(Digito.fromValue(digito));
	}

	public String[] getDigits() {
		return this.valor.split("");
	}

}
