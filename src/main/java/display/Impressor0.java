package display;

public class Impressor0 extends Impressor {
	
	private static String linha1 = " _ ";
	private static String linha2 = "| |";
	private static String linha3 = "|_|";
	private static String linhas[] = {linha1, linha2, linha3};

	@Override
	public String linha(int linha) {
		return linhas[linha];
	}
}
