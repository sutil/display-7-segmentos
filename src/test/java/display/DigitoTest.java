package display;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class DigitoTest {
	
	@Test
	public void testeZero() {
		Digito zero = Digito.fromValue("0");
		
		assertEquals(Digito.ZERO, zero);
	}
	
	@Test
	public void testeUm() {
		Digito digito  = Digito.fromValue("1");
		
		assertEquals(Digito.UM, digito);
	}

}
