package display;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class Impressor1Test {
	
	@Test
	public void impressor() {
		Impressor1 impressor1 = new Impressor1();
		
		assertEquals(" ", impressor1.linha(0));
		assertEquals("|", impressor1.linha(1));
		assertEquals("|", impressor1.linha(2));
	}

}
