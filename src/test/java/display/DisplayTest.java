package display;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class DisplayTest {
	
	@Test
	public void ImprimeZero() {
		String saida = new Display("0").imprime();
		
		String saidaEsperada = 
				" _ \n"
			  + "| |\n"
			  + "|_|\n"
					;
		
		assertEquals(saidaEsperada, saida);
	}
	
	@Test
	public void ImprimeUm() {
		String saida = new Display("1").imprime();
		
		String saidaEsperada = 
				" \n"
			  + "|\n"
			  + "|\n";
		
		assertEquals(saidaEsperada, saida);
	}
	
	@Test
	public void Imprime01() {
		String saida = new Display("01").imprime();
		
		String saidaEsperada = 
				" _  \n"
			  + "| ||\n"
			  + "|_||\n";
		
		assertEquals(saidaEsperada, saida);
	}
	
	@Test
	public void Imprime2() {
		String saida = new Display("2").imprime();
		
		String saidaEsperada = 
				" _ \n"
			  + " _|\n"
			  + "|_ \n";
		
		assertEquals(saidaEsperada, saida);
	}
	
	
	@Test
	public void getDigits() {
		String[] digits = new Display("10").getDigits();
	
		assertEquals(2, digits.length);
		assertEquals("1", digits[0]);
		assertEquals("0", digits[1]);
	}

}
