package display;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class Impressor0Test {
	
	@Test
	public void imprime() {
		assertEquals(" _ ", new Impressor0().linha(0));
		assertEquals("| |", new Impressor0().linha(1));
		assertEquals("|_|", new Impressor0().linha(2));
	}

}
